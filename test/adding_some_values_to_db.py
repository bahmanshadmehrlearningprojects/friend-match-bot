from mongoengine import *
connect('chatbot')
from random import randint

def random_string():
	letters = 'abcdefghigklmnopqrstuvwxyz'
	x = ''
	for i in range(12):
		x += letters[randint(0, 25)]
	return "".join(x)


import Models

for i in range(20):
	state = Models.State(name = random_string(), code = 200 + i)
	state.save()

for i in range(10000):
	genders = ["M", "F", "O"]
	gender = genders[randint(0, 2)]
	user = Models.Users()
	c = randint(0, 200000)
	if Models.Users.objects(chat_id = c):
		continue
	user.chat_id = c
	user.username = random_string()
	user.name = random_string()
	user.gender = gender
	user.state = Models.State.objects(code = randint(200, 219)).first()
	user.initilize()
	user.save()
	print(i)

for i in range(10):
	pre = Models.Premmisions(premmision_name = random_string(), premmision_code = i + 5000 + 1)
	pre.save()

for i in range(3):
	z = []
	for x in range(3):
		z += Models.Premmisions.objects(premmision_code = randint(5001, 5010))
	admin = Models.Admins(chat_id = randint(0, 5000), premmisions = z)

for user in Models.Users.objects():
	answe = []
	for i in range(20):
		answe.append(randint(1, 20))

	user.answers = answe
	user.save()
