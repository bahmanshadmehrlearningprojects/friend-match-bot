from Contants import my_tocken, owner_id
from random import randint
import asyncio
import telepot
from telepot.aio.loop import MessageLoop
from telepot.aio.delegate import (
per_chat_id_in, per_application, call, create_open, pave_event_space)

import Models

from mongoengine import *
connect('chatbot')

class OwnerHandler(telepot.aio.helper.ChatHandler):
    def __init__(self, seed_tuple, **kwargs):
        super(OwnerHandler, self).__init__(seed_tuple, **kwargs)


    async def on_chat_message(self, msg):
        content_type, chat_type, chat_id = telepot.glance(msg)

        print(chat_id)
        if content_type != 'text':
            await self.sender.sendMessage('متوجه پیغام نمیشم، یه دستور درست ارسال  کن...')
            return

        command = msg['text'].strip().lower()

        if command == '/statics':
            user_count = Models.Users.objects.count()
            banned_user_count = Models.Users.objects(completely_banned = True).count()
            active_user = Models.Users.objects(active = True).count()
            unanswered_users = Models.Users.objects(has_answered = False).count()

            message = "آمار دیتابیس:" + '\n' + \
                        "تعداد اعضا: " + str(user_count) + '\n' + \
                        "تعداد اعضای بن شده: " + str(banned_user_count) + '\n' + \
                        "تعداد اعضای فعال: " + str(active_user) + '\n' + \
                        "تعداد اعضایی که به سوال ها پاسخ نداده اند: " + str(unanswered_users)

            await self.sender.sendMessage(message)
            return

        if command.startswith('/bann_user') or command.startswith('/unbann_user'):
            command_parts = command.strip().split(' ')

            if len(command_parts) != 2:
                await self.sender.sendMessage('دستور به درستی وارد نشده است...')
                return

            if command.startswith('/bann_user'):
                status = Models.Users.objects(username = command_parts[1]).update(completely_banned = True)
                await self.sender.sendMessage('یوزر با موفقیت بن شد...')

            elif command.startswith('/unbann_user'):
                status = Models.Users.objects(username = command_parts[1]).update(completely_banned = False)
                await self.sender.sendMessage('یوزر با موفقیت از بن درآمد...')

            if not status:
                await self.sender.sendMessage('یوزر وارد شده وجود ندارد...')
            return



        await self.sender.sendMessage('متوجه پیغام نمیشم، یه دستور درست ارسال  کن...')


class ChatBox(telepot.aio.DelegatorBot):
    def __init__(self, token, owner_id):
        self._owner_id = owner_id

        print(self._owner_id)

        super(ChatBox, self).__init__(token, [
            pave_event_space()(
                per_chat_id_in([self._owner_id]), create_open, OwnerHandler, timeout=20),

            # For senders never seen before, send him a welcome message.
            # (self.is_new, call(self._send_chat_id)),
])

    def is_new(self, msg):
        if telepot.is_event(msg):
            return None

        return []

    async def _send_chat_id(self, seed_tuple):
        chat_id = seed_tuple[1]['chat']['id']
        print('Sending Chat ID')
        await self.sendMessage(chat_id, chat_id)



bot = ChatBox(my_tocken, owner_id)
loop = asyncio.get_event_loop()

loop.create_task(MessageLoop(bot).run_forever())
print("Start...")
loop.run_forever()
