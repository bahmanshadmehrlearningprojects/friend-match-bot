import Models
from math import sqrt
def find_ten_best_matches(main_user):
	# print(main_user)
	data = main_user.answers
	ten_closest_match = {}
	ten_closest_match['biggest'] = 0
	for i in range(10):
		ten_closest_match[i] = 100
	for user in Models.Users.objects():
		if user.pk == main_user.pk:
			continue

		row = user.answers

		sum_of_powers = 0
		for i in range(len(data)):
			sum_of_powers += (data[i] - row[i])**2

			
		if sqrt(sum_of_powers) < ten_closest_match[ten_closest_match['biggest']]:
			del ten_closest_match[ten_closest_match['biggest']]
			ten_closest_match[user.chat_id] = sqrt(sum_of_powers)
			r = 0
			for key, value in ten_closest_match.items():
				if key == 'biggest':
					continue
				if value > r:
					r = value
					ten_closest_match["biggest"] = key
	return ten_closest_match