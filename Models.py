from mongoengine import *

GENDERS = ('M', 'F', 'O')

class State(Document):
	name = StringField(max_length = 100, required = True)
	code = IntField(required = True)

class Users(Document):
	active = BooleanField(default = False, required = True)
	chat_id = IntField(required = True, unique = True)
	username = StringField(max_length = 30, unique = True)
	has_answered = BooleanField(default = False, required = True)
	coins = IntField(min_value = 0, default = 0)
	answers = ListField(IntField(min_value = 0, max_value = 20))
	name = StringField(max_length = 100, required = True)
	gender = StringField(max_length = 2, choices = GENDERS)
	state = ReferenceField(State)
	invite_link = StringField(max_length = 200, unique = True)
	self_compare_link = StringField(max_length = 200, unique = True)
	banned_by = ListField(ReferenceField('self'))
	completely_banned = BooleanField(default = False, required = True)

	def initilize(self):
		self.invite_link = 'telegram.me/myBot?start=invite' + self.username
		self.self_compare_link = 'telegram.me/myBot?start=compare' + self.username
		self.answers = [0] * 20

class Premmisions(Document):
	premmision_name = StringField(max_length = 100, required = True, unique = True)
	premmision_code = IntField()

class Admins(Document):
	chat_id = IntField(required = True, unique = True)
	premmisions = ListField(ReferenceField(Premmisions))
